# -*- encoding=utf-8 -*-

"""
    This is an implementation of linear least-squares regression
    using linear algebra and matrix inversion.
"""

import numpy as np
import matplotlib.pyplot as plt

a = 1
b = 2

X = np.array([0, 1, 2, 3])
Y = a * X + b + np.random.normal(scale=0.1, size=len(X))

fig = plt.figure()
plt.plot(X, Y)
fig.show()
plt.waitforbuttonpress()

# "X" is the input array of length N.
#
# We want to calculate the parameters a and b for a function:
#
# g(x) = a + b x
#
# such that the sum over k = 1..N  of
# err(a, b) = (g(X_k) - Y_k) ^ 2
# is minimal
#
# This is the case when both differentials
#
# \delta err / \delta a = 0 and
# \delta err / \delta b = 0
#
# and hence the linear equation system
#
# sum over k: a + b x_k - y_k = 0
#             N a + b sum x_k - sum y_k = 0
# sum over k: a x_k + b (x_k)^2 - y_k x_k = 0
#             a sum x_k + b sum x_k^2 - sum x_k y_k = 0
#
# We can write this in matrix form
# using <x, y> for the scalar product
# M x c - d = 0
#          N      sum x_k        a       sum y_k
#     (                   ) x  (   ) - (         ) = 0
#       sum x_k    <x, x>        b        <x, y>
#
M = np.matrix([
    [X.sum(), (X * X).sum()],
    [len(X), X.sum()]
])
c = np.array([(X * Y).sum(), Y.sum()])

# Hence we get c from: M^-1 d by matrix-inverting M
d = np.linalg.inv(M) @ c
