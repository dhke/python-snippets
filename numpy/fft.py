# -*- coding: utf-8 -*-

"""
    This contains an implementation of the fast fourier transform
    using a simple implementation of the Cooley–Tukey algorithm.
"""

import numpy as np
# import pandas as pd
import matplotlib.pyplot as plt


def fft_direct(x):
    """
        direct implementation of the discrete fourier transform.

        This is for reference and not meant for general use.
    """
    N = len(x)
    X = np.zeros(N, dtype=np.complex)
    K = np.arange(N)
    for k in K:
        a = np.complex(0)
        for n in range(0, N):
            a += x[n] * np.exp((-2 * np.pi * 1j / N) * n * k)
            X[k] = a
    return X


def fft_copy(x):
    """
        Simple, recursive copying version of the Fast Fourier Transform
        This creates a copy of the initial array
    """
    size = len(x)
    size2 = size // 2
    if size == 1:
        X = np.array(x)
    else:
        # even terms
        Xe = fft_copy(x[::2])
        # odd terms
        Xo = fft_copy(x[1::2])
        X = np.zeros(size, dtype=np.complex)
        for k in range(0, size2):
            b = np.exp(-2j * np.pi * (k / size))
            X[k] = Xe[k] + b * Xo[k]
            X[k + size2] = Xe[k] - b * Xo[k]

    return X

Ndisp = 1024
N = 1024


def f(t):
    return np.sin(4 * np.pi * t) + 0.1 * np.sin(512 * np.pi * t)

t = np.linspace(0, 1, N)
x = f(t)

t_disp = np.linspace(0, 1, Ndisp)
x_disp = f(t)

print("NUMPY:")
Xnp = np.fft.fft(x)
# print("SLOW:")
# Xslow = fft_direct(x)
print("FAST:")
Xfft_copy = fft_copy(x)
print("DONE")

K = np.arange(N)

fig = plt.figure()
plt.plot(t_disp, x_disp)
fig.savefig('x_orig.png')

fig = plt.figure()
plt.scatter(K, np.real(Xnp))
fig.savefig('Xnp.png')

#fig = plt.figure()
#plt.scatter(K, Xslow)
#fig.savefig('Xslow.png')

fig = plt.figure()
plt.scatter(K, np.real(Xfft_copy))
fig.savefig('Xfft_copy.png')

fig = plt.figure()
x_rev = np.fft.ifft(Xfft_copy, n=Ndisp)
plt.plot(t_disp, x_rev)
fig.savefig('Xfft_copy_ifft.png')
