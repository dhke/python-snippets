# dhke's Python Snippets

This repository contains various categorized, but otherwise unsorted Python snippets
that I find useful from time to time.

## License

These scripts are licensed under [CC-BY-SA 4.0 ![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

## Categories

This repository contains some subdirectories which represent categories.

### docs

Documents with write-ups and explanations. These are not necessarily in Python.

### crypto

Cryptographic protocols or algorithms.

### numpy

Utilities and scripts for use in combination with [numpy](http://www.numpy.org/).

### pandas

Utilities and scripts for use in combination with the [pandas](https://pandas.pydata.org/) data analysis library.

### pandas

Utilities and script for use in combination with the [scipy](https://www.scipy.org/) data analysis library.
